# 个人简介
## 有时未能及时回复，请联系微信：shuai9378
> 深耕电商3年，擅长电商类、公众号、小程序、App服务、后台管理系统服务过很多客户。领导过20人小团队，从原型构建到项目交付。熟悉项目的管理、需求对接。 
> 平时喜爱阅读技术类书籍，与人交流当前技术、产品，也有自己的分享 
> 坚持客户第一的位置，把服务好客户需求作为我的目标。


# 项目经验
### 微耶微信聚合平台
> 技术栈：PHP、ThinkPHP、JavaScript, Bootstrap, Nginx

> 项目简介：微耶聚合平台是一个囊括支付， 视频， 音频， 图片， 即时通讯， 邮件短信， 地图， 数据可视化， 推送， 社交分享， 第三方登录， 二维码扫描

> 项目截图：
![图片](src/5dc56a46ab822d84441044ff197a8ed4.jpg)
![图片](src/ade65e1b3b9e312f4b6363d3a03fbb77.jpg)
![图片](src/21e11dcd6c74489af338a62447e77235.jpg)

### 手商云电商平台
> 技术栈：Java, PHP, object-c, javascript, nginx, mysql, redis

> 项目简介： 手商云是一款针对家庭的在线购物平台.囊括：分销机制、支付， 音频， 图片， 文件处理， 邮件短信， 数据可视化， 推送， 第三方登录， 二维码扫描， 统计分析

> 项目截图：
![图片](src/ae8b87e4eb362153b4b20e0518e04b15.jpg)
![图片](src/fa62753083d220894a1f1a8d4e098b44.jpg)
![图片](src/42f26b51d9c30a0f8a582bf34ae4fcfd.jpg)


### 犬社小程序
> 技术栈：Python、Tornado、JavaScript、HTML5、CSS、Redis、MySql、Nginx

> 项目简介：犬社是一款以狗狗社交为基础的社区小程序，包涵视频， 音频， 图片， 推送， 社交分享， 第三方登录， 统计分析

> 项目截图：
![图片](src/recordsay1528504356%20(1).jpg)
![图片](src/recordsay1528504356.jpg)


### 练歌房小程序
> 技术栈：Python、Tornado、MySql、Nginx、Redis

> 项目简介：练歌房是一款在线教唱的小程序。包括歌曲、课程、课时在线观看，与老师交流。

> 项目截图：
![图片](src/2WechatIMG65.jpeg)
![图片](src/2WechatIMG66.jpeg)
![图片](src/2WechatIMG67.jpeg)
![图片](src/2WechatIMG68.jpeg)


### 短信通知云平台
> 技术栈：PHP、Java、MySql、Nginx、Redis、HTML5、JavaScript

> 项目简介：一款在线短信群发平台

> 项目截图：
![图片](src/3WechatIMG2977.jpeg)
![图片](src/3WechatIMG2976.jpeg)
![图片](src/3WechatIMG2975.jpeg)
![图片](src/3WechatIMG2973.jpeg)


### 抽奖小程序
> 技术栈：Node.js、HTML5、CSS、MySql、Nginx

> 项目简介：一款在线抽奖小程序，与朋友分享。增加流传度

> 项目截图：
![图片](src/Page品牌活动.png)
![图片](src/Page实物获奖.png)
![图片](src/Page我的抽奖记录.png)
![图片](src/Page赞助商介绍-02.png)


### 助你好孕公众号
> 技术栈：PHP、ThinkPHP、MySql、Redis、Nginx

> 项目简介：一款在线助孕预约咨询平台

> 项目截图：
![图片](src/4WechatIMG71.jpeg)
![图片](src/4WechatIMG72.jpeg)
![图片](src/4WechatIMG70.jpeg)


### 如此云单
> 技术栈：PHP、ThinkPHP、MySql、Redis、Nginx、android、jpush, ping++

> 项目简介：一款针对机房运维的在线派单平台

> 项目截图：
![图片](src/343cdb5705cb3f61f5daa529c893a911.png)
![图片](src/aa122f40ca62f1e38b0ff30fa36de5fa.png)
![图片](src/1aa42ed7d070caee659dba52051fb5a5.png)

